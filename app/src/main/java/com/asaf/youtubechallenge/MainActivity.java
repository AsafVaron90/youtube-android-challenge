package com.asaf.youtubechallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.asaf.youtubechallenge.fragments.ListFragment;
import com.asaf.youtubechallenge.interfaces.ResultCallback;
import com.asaf.youtubechallenge.java.Playlist;
import com.asaf.youtubechallenge.java.Server;
import com.asaf.youtubechallenge.java.Video;
import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    List<Playlist> mPlaylistList = new ArrayList<>();

    private JSONArray mResponse;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fm = getSupportFragmentManager();
        // get data from server and init in lists
        getYoutubePlaylistData(new ResultCallback() {
            @Override
            public void onResult(Object data, String e) {
                if (e == null) {
                    // load fragment
                    loadFragment();
                } else
                    Toast.makeText(MainActivity.this, e, Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void loadFragment() {
        ListFragment list = ListFragment.newInstance();
        fm.beginTransaction().replace(R.id.main_container, list, "PlayListFrag")
                .commit();
    }

    // get the data from url
    private void getYoutubePlaylistData(final ResultCallback callback) {
        // use Volley to request the data
        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET,
                Server.YOUTUBE_LIST_URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: response= " + response);
                        try {
                            // get the json array
                            mResponse = response.getJSONArray("Playlists");
                            createData(mResponse, callback);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: ERR:" + error.getLocalizedMessage());
                        callback.onResult(null, error.getMessage());
                    }
                }
        );
        Server s = Server.getInstance();

        // clear the cache
        s.getRequestQueue().getCache().invalidate(Server.YOUTUBE_LIST_URL, true);

        // add request to queue
        s.getRequestQueue().add(req);
    }

    private void createData(JSONArray array, ResultCallback callback) {
        Log.d(TAG, "createData: array=" + array);

        // child list
        ArrayList<Video> videoList;

        try {
            for (int i = 0; i < array.length(); i++) {
                Log.d(TAG, "createData: i=" + i);
                // get the json objects
                JSONObject obj = array.getJSONObject(i);

                // validate playlist
                if (obj.has("ListTitle") && obj.has("ListItems")) {
                    String playlistTitle = obj.getString("ListTitle");
                    Log.d(TAG, "createData: playlist title= " + playlistTitle);

                    // set the new video items
                    videoList = new ArrayList<>();

                    // get the inner json array for each object
                    JSONArray items = obj.getJSONArray("ListItems");
                    for (int j = 0; j < items.length(); j++) {
                        Log.d(TAG, "createData: j=" + j);
                        JSONObject o = items.getJSONObject(j);

                        // set the inner data
                        if (o.has("Title") && o.has("link") && o.has("thumb")) {
                            // create a new child object
                            Video v = new Video(o.getString("Title"), o.getString("link"), o.getString("thumb"));
                            Log.d(TAG, "createData: video add to list:\n" + v.toString());
                            videoList.add(v);
                        }
                    }
                    Playlist p = new Playlist(playlistTitle, videoList);
                    mPlaylistList.add(p);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        callback.onResult(mPlaylistList, null);
    }

    // start the video
    public void showYouTubeVideo(String videoUrl) {

        Log.d(TAG, "showYouTubeVideo: videoID: " + videoUrl);
        Intent intent = new Intent(MainActivity.this, YouTubePlayerActivity.class);

        // Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, YouTubeUrlParser.getVideoId(videoUrl));

        // Youtube player style (DEFAULT as default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);

        // Screen Orientation Setting (AUTO for default)
        // AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);

        // Show audio interface when user adjust volume (true for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);

        // If the video is not playable, use Youtube app or Internet Browser to play it
        // (true for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);

        // Animation when closing youtubePlayerActivity (none for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, R.anim.slide_up);
        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, R.anim.slide_out_down);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public List<Playlist> getPlaylist() {
        Log.d(TAG, "getPlaylist: list= " + mPlaylistList.toString()
                + " \nlist size=" + mPlaylistList.size());
        return mPlaylistList;
    }
}
