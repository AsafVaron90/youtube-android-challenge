package com.asaf.youtubechallenge;

import android.app.Application;

import com.asaf.youtubechallenge.java.Server;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // init volley class
        Server.getInstance().init(this);
    }
}
