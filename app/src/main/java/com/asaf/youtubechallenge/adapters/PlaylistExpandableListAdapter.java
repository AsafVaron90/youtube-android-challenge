package com.asaf.youtubechallenge.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asaf.youtubechallenge.R;
import com.asaf.youtubechallenge.java.Playlist;
import com.asaf.youtubechallenge.java.Video;
import com.asaf.youtubechallenge.view_holders.PlaylistViewHolder;
import com.asaf.youtubechallenge.view_holders.VideoItemViewHolder;
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

public class PlaylistExpandableListAdapter extends ExpandableRecyclerAdapter<PlaylistViewHolder, VideoItemViewHolder> {

    private static final String TAG = PlaylistExpandableListAdapter.class.getSimpleName();

    private final LayoutInflater mInflater;
    private Context mContext;

    public PlaylistExpandableListAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        Log.d(TAG, "PlaylistExpandableListAdapter: created");
    }

    // onCreate
    @Override
    public PlaylistViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        Log.d(TAG, "onCreateParentViewHolder: ");
        View view = mInflater.inflate(R.layout.playlist_item, viewGroup, false);
        return new PlaylistViewHolder(view);
    }

    @Override
    public VideoItemViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        Log.d(TAG, "onCreateChildViewHolder: ");
        View view = mInflater.inflate(R.layout.video_item, viewGroup, false);

        return new VideoItemViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(PlaylistViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        Log.d(TAG, "onBindParentViewHolder: ");
        Playlist playlist = (Playlist) parentListItem;
        parentViewHolder.bind(playlist);
    }

    @Override
    public void onBindChildViewHolder(VideoItemViewHolder childViewHolder, int i, Object childObject) {
        Log.d(TAG, "onBindChildViewHolder: ");
        Video video = (Video) childObject;
        childViewHolder.bind(mContext, video);
    }
}
