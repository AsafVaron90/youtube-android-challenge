package com.asaf.youtubechallenge.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asaf.youtubechallenge.MainActivity;
import com.asaf.youtubechallenge.R;
import com.asaf.youtubechallenge.adapters.PlaylistExpandableListAdapter;
import com.asaf.youtubechallenge.java.Playlist;
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;

import java.util.List;

public class ListFragment extends Fragment implements ExpandableRecyclerAdapter.ExpandCollapseListener {

    private static final String TAG = ListFragment.class.getSimpleName();

    private RecyclerView mRecycleView;
    private List<Playlist> playlistList;

    private PlaylistExpandableListAdapter mPlaylistExpandableListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static ListFragment newInstance() {
        ListFragment frag = new ListFragment();
        Bundle args = new Bundle();
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View root = inflater.inflate(R.layout.list_fragment, container, false);

        mRecycleView = (RecyclerView) root.findViewById(R.id.recycle_view);

        playlistList = ((MainActivity) getActivity()).getPlaylist();

        mPlaylistExpandableListAdapter = new PlaylistExpandableListAdapter(getActivity(), playlistList);

        mPlaylistExpandableListAdapter.setExpandCollapseListener(this);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecycleView.setLayoutManager(mLayoutManager);

        mRecycleView.setAdapter(mPlaylistExpandableListAdapter);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPlaylistExpandableListAdapter.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        mPlaylistExpandableListAdapter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onListItemExpanded(int position) {
        Playlist expandedPlaylist = playlistList.get(position);
        Log.d(TAG, "onListItemExpanded: playlist=" + expandedPlaylist);
    }

    @Override
    public void onListItemCollapsed(int position) {
        Playlist collapsedPlaylist = playlistList.get(position);
        Log.d(TAG, "onListItemCollapsed: playlist=" + collapsedPlaylist);
    }
}
