package com.asaf.youtubechallenge.interfaces;

public interface ResultCallback<T> {
    void onResult(T data, String e);
}
