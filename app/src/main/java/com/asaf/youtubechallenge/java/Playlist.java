package com.asaf.youtubechallenge.java;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

// parent class
public class Playlist implements ParentListItem {


    private List mChildrenList;
    private String mPlayListTitle;

    public Playlist(String playlistTitle, List childrenList) {
        this.mPlayListTitle = playlistTitle;
        this.mChildrenList = childrenList;
    }

    @Override
    public List<?> getChildItemList() {
        return mChildrenList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public String getPlayListTitle() {
        return mPlayListTitle;
    }

    @Override
    public String toString() {
        return "playlist = " + mPlayListTitle + "\ndata: " + mChildrenList;
    }
}
