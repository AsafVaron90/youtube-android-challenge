package com.asaf.youtubechallenge.java;

// child class
public class Video {

    private String videoTitle;
    private String videoLink;
    private String imageUrl;

    public Video(String videoTitle, String videoLink, String imageUrl) {
        this.videoTitle = videoTitle;
        this.videoLink = videoLink;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return videoTitle;
    }

    public String getVideoLink() {
        return videoLink;
    }

    @Override
    public String toString() {
        return "Video data:"
                + "\ntitle= " + videoTitle
                + "\nlink= " + videoLink
                + "\nimage url= " + imageUrl;
    }
}
