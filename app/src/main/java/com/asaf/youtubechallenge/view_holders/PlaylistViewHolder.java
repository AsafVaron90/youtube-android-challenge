package com.asaf.youtubechallenge.view_holders;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.asaf.youtubechallenge.R;
import com.asaf.youtubechallenge.java.Playlist;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

public class PlaylistViewHolder extends ParentViewHolder {
    private static final String TAG = PlaylistViewHolder.class.getSimpleName();
    public TextView mTitle;

    public PlaylistViewHolder(View itemView) {
        super(itemView);

        // find the views for parent object
        mTitle = (TextView) itemView.findViewById(R.id.playlist_title);
        Log.d(TAG, "PlaylistViewHolder: created");
        mTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpanded())
                    collapseView();
                else
                    expandView();
            }
        });
    }

    @Override
    public boolean shouldItemViewClickToggleExpansion() {
        return false;
    }

    public void bind(Playlist playlist) {
        mTitle.setText(playlist.getPlayListTitle());
    }
}
