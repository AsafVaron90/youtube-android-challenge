package com.asaf.youtubechallenge.view_holders;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.asaf.youtubechallenge.MainActivity;
import com.asaf.youtubechallenge.R;
import com.asaf.youtubechallenge.java.Video;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bumptech.glide.Glide;


public class VideoItemViewHolder extends ChildViewHolder {

    private static final String TAG = VideoItemViewHolder.class.getSimpleName();
    private View mView;

    public TextView mTitle;
    public ImageView mImageView;
    public TextView mLink;

    public VideoItemViewHolder(View itemView) {
        super(itemView);

        mView = itemView;
        // find the views for child object
        mTitle = (TextView) itemView.findViewById(R.id.video_title);
        mLink = (TextView) itemView.findViewById(R.id.video_link);
        mImageView = (ImageView) itemView.findViewById(R.id.video_img);
        Log.d(TAG, "VideoItemViewHolder: created");

    }

    public void bind(final Context context, final Video video) {
        mTitle.setText(video.getTitle());
        mLink.setText(video.getVideoLink());

        // load the thumb
        Glide.with(context)
                .load(video.getImageUrl())
                .into(mImageView);

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)context).showYouTubeVideo(video.getVideoLink());
            }
        });

    }
}
